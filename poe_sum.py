import re
import sys

filters = {
    'none':         [],
    'spells':       ['Spells'],
    'bows':         ['Bows'],
    'wands':        ['Wands'],
    'claws':        ['One Handed Melee Weapons', 'Claws'],
    'daggers':      ['One Handed Melee Weapons', 'Daggers'],
    '1h_swords':    ['One Handed Melee Weapons', 'Swords'],
    '1h_axes':      ['One Handed Melee Weapons', 'Axes'],
    '1h_maces':     ['One Handed Melee Weapons', 'Maces'],
    '2h_swords':    ['Two Handed Melee Weapons', 'Swords'],
    '2h_axes':      ['Two Handed Melee Weapons', 'Axes'],
    '2h_maces':     ['Two Handed Melee Weapons', 'Maces'],
    'staves':       ['Two Handed Melee Weapons', 'Staves'],
}

dual_wielding = ['wands', 'claws', 'daggers', '1h_swords', '1h_axes', '1h_maces']

re_compiled = [
    re.compile('([\d.]+)% increased (.*) with (.*)'),
    re.compile('([\d.]+)% increased (.*) for (.*)'),
    re.compile('([\d.]+)% increased (.*) while (.*)'),
    re.compile('([\d.]+)% increased (.*)'),
]

def parse(line):
    for exp in re_compiled:
        search = exp.match(line)
        if search:
            try:
                return float(search.group(1)), search.group(2), search.group(3)
            except:
                return float(search.group(1)), search.group(2), None
    return 0, None, None

def main():
    filename = sys.argv[1]
    weapon   = sys.argv[2] if len(sys.argv) > 2 else 'none'
    dw       = len(sys.argv) > 3  # enable dual wielding if there's a third param
    summary  = {}
    with open(filename) as f:
        for line in f:
            percent, category, filter = parse(line)
            if not category:
                continue
            key = category
            if dw and filter == 'Dual Wielding' and weapon in dual_wielding:
                pass
            elif filter and not filter in filters[weapon]:
                key = '%s with %s' % (category, filter)
            if not key in summary:
                summary[key] = 0
            summary[key] += percent
    keys = sorted(summary)
    for key in keys:
        print('%s: %3.1f%%' % (key, summary[key]))

if __name__ == '__main__':
    main()
